Makstukaista module for Drupal Commerce
===============================

Note: to accept payments with Maksukaista module, you need to have an active contract with Maksukaista

Installation:
Module is installed the same way as other Drupal modules. Go to Modules page in administrator panel, and either directly upload the 
module, or insert link to the file in drupal.org ftp server. After installation is complete, enable the module from module listing and 
save module settings

After module has been enabled, go to Maksukaista module settings and input your private key and submerchant id. These can be found
from Maksukaista merchant panel at www.maksukaista.fi . 
